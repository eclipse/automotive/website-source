---
title: "Eclipse Automotive"
headline: "Eclipse Automotive"
description: "Eclipse Automotive - The Automotive Community for Open Innovation and Collaboration"
date: 2024-11-08
type: "election"
footer_class: "footer-darker"
---

# Open Collaboration for the Automotive Industry

The automotive industry is undergoing massive transformation with emerging automated, connected, and electric automotive technologies enabling new mobility services in a shared economy. In turn, these innovations are altering the way automakers, their partners, and customers create value and benefit from vehicles and transportation.

## Eclipse Automotive Top Level Project

 The **Eclipse Automotive** Top-Level Project provides a space for open source projects to explore ideas and technologies addressing challenges in the automotive, mobility and transportation domain. It is the common goal of this project to provide tools and composable building blocks to empower the development of solutions for the mobility of the future.

## The Growing Eclipse Automotive Ecosystem

### Automotive industry leaders collaborate at the Eclipse Foundation

- 35+ Members of Eclipse Automotive Working Groups• 6 of the 20 Leading Automakers
- 3 of the Largest Tier 1 and Tier 2 Suppliers
- Leading Automotive Engineering Companies, such as Vector Informatik, Gigatronik, Mueller-BBM, AVL, and others
- Key research and government organizations, including Fraunhofer, Offis, DLR, DFKI, Fortiss, Virtual Vehicle, and others

The Eclipse Foundation delivers an open, vendor-neutral governance framework, and IP cleansing processes for OEMs, Tier 1 and Tier 2 suppliers and players in the automotive ecosystem like Audi AG, BMW Group, Robert Bosch GmbH, Daimler AG, and Siemens AG, who wish to collaborate on core capabilities below the value line to solve their technology needs. In doing so, they free up scarce organizational resources to accelerate product development that enables intellectual property sharing without the threat of antitrust and regulatory challenges, increased focus on delivering differentiating features faster, and pooling of engineering resources.

## The Opportunity

### Participation in Eclipse Foundation Automotive Working and Interest Groups enables our members to

- Use the work that has been created by the existing Working Groups.
- Contribute to the current projects by participating in the Working Groups
- Share intellectual property without the threat of antitrust and regulatory challenges
- Enable the creation of a common open platform for automotive and transportation software quality assurance and testing
- Share costs below the value line and focus on investing scarce development resources on building differentiated features
- Accelerate product development and thereby improve time-to-revenue
- Leverage open collaboration and shared engineering resources as a force multiplier for members at a time of a critical developer shortage
- Enable ecosystem participants alike to safely collaborate across value chains and industries to develop new business models and revenue streams
- Propose new Working Groups to foster collaboration in an open and transparent environment enabled by the Eclipse Foundation’s governance rules.

## Key Eclipse Foundation Automotive Working and Interest Groups

 Here are the current Eclipse Working and Interest Groups dealing with Automotive stuff:

### openMDM

![openMDM](https://www.eclipse.org/org/workinggroups/assets/images/wg-openmdm.svg)

Test systems in the automotive industry are expensive to implement and operate. In addition, verification procedures during automotive product development generate an ever-increasing volume and diversity of test data. The openMDM Working Group drives the Eclipse MDM|BL project, an open source, vendor-independent, and server-based application for the management of ASAM ODS compliant measurement data. It offers generally required functionalities, such as data storage, full-text search, data import/export and selection of test data.

Learn more @ <https://www.openmdm.org>

### openMobility

![openMobility](https://www.eclipse.org/org/artwork/images/open-mobility-logo.svg)

The openMobility Interest Group drives the evolution and broad adoption of mobility modeling and simulation technologies. It is based on Eclipse Simulation of Urban Mobility (SUMO), a framework that supports the detailed simulation of the movement of vehicles, people, as well as their communication patterns. openMobility’s efforts will be critical in testing driver assistance systems, predicting and optimizing traffic as well as evaluating new business models, such as Mobility-as-a-Service.

Learn more @ <https://openmobility.eclipse.org>

### openPASS

![openPASS](https://www.eclipse.org/org/artwork/images/open-pass-logo.svg)

The need for virtual simulation of advanced driver assistance systems, partially automated driving functions, and assessment of safety effects in traffic has given rise to the creation of the openPASS Working Group. openPASS specifies and builds the open source SIM@openPASS platform containing various methods and tools for the prospective evaluation of advanced driver assistance systems with respect to traffic safety.

Learn more @ <https://openpass.eclipse.org>

### Software defined Vehicle (SDV)

![SDV](https://www.eclipse.org/org/artwork/images/sdv-logo.svg)

An open technology platform for the software-defined vehicle of the future; focused on accelerating innovation of automotive-grade in-car software stacks using open source and open specifications developed by a vibrant community.

Learn more @ <https://sdv.eclipse.org/>

## Join us

 To join an existing Eclipse Foundation Automotive Working Group, or find out how to create a new Eclipse Working Group, please contact the Membership Coordination Team at <membership@eclipse.org>.
