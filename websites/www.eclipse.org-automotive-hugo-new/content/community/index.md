---
title: "Community"
headline: "Community"
date: 2024-11-08
#description: "Community"
#hide_sidebar: false
#hide_breadcrumb: true
---

[Automotive Project Page](https://projects.eclipse.org/projects/automotive)

* [Who is involved](https://projects.eclipse.org/projects/automotive/who)

* [Governance / Eclipse Development Process](https://projects.eclipse.org/projects/automotive/governance "Automotive - Governance")

[Wiki](https://wiki.eclipse.org/Automotive "Automotive Wiki")

[Mailing list](https://accounts.eclipse.org/mailing-list/automotive-pmc "Automotive Mailing list")
