---
title: "Documentation"
date: 2024-11-08T10:30:00-04:00
headline: "Documentation"
type: "documentation"
footer_class: "footer-darker"
---

Sign up on the [Automotive mailing list](https://accounts.eclipse.org/mailing-list/automotive-pmc) for the latest news.


### Links

* [Top-Level Project Charter](https://projects.eclipse.org/projects/automotive/charter)

* [Wiki](https://wiki.eclipse.org/Automotive)