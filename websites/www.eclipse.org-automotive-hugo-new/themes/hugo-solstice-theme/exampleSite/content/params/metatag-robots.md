---
title: Metatag Robots Config Parameter
hide_sidebar: true
metatag_robots: noindex, nofollow
---

The `metatag_robots` config parameter grants the ability to add a "robots" metatag with the set `content` attribute.

If blank, no meta tag will be created for "robots".

Usage:
```md
title: Metatag Robots Config Parameter
hide_sidebar: true
metatag_robots: noindex, nofollow
```

