---
title: Table of Contents
date: 2024-01-16
---

{{< table_of_contents >}}

## Introduction

This is a sample page to demonstrate the use of the `table_of_contents`
shortcode.

## Usage

The `table_of_contents` shortcode is used to generate a table of contents
based on the headings in the current page. The table of contents is generated
using headings.

The table of contents is generated using the following syntax:

```
{{</* table_of_contents  */>}}
```

### Parameters

The `table_of_contents` shortcode has no parameters.
