---
title: "Newsroom Resources" 
hide_sidebar: true
---

You can display resources from the [Eclipse Newsroom](https://newsroom.eclipse.org/) 
using the `newsroom/resources` shortcode.

{{< newsroom/resources wg="ecd_tools" title="White Papers" type="white_paper" >}}
{{< newsroom/resources wg="ecd_tools" title="Case Studies" type="case_study" >}}
{{< newsroom/resources wg="ecd_tools" title="Survey Reports" type="survey_report" >}}

---

## Usage 

Code used on this page:

```
{{</* newsroom/resources wg="ecd_tools" title="White Papers" type="white_paper" */>}}
{{</* newsroom/resources wg="ecd_tools" title="Case Studies" type="case_study" */>}}
{{</* newsroom/resources wg="ecd_tools" title="Survey Reports" type="survey_report" */>}}
```

