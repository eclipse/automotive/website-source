---
title: Demonstrating yearly_sections_enabled Param
hide_sidebar: true
---

The `yearly_sections_enabled` parameter splits meeting minutes into yearly sections if set to `true`.

```
{{</* eclipsefdn_meeting_minutes yearly_sections_enabled="true" */>}}
```

{{< eclipsefdn_meeting_minutes yearly_sections_enabled="true" >}}

