---
title: "Youtube"
date: 2019-04-17T15:52:27-04:00
description: ""
categories: []
keywords: []
slug: ""
aliases: []
toc: false
draft: false
---

You can add a youtube video to a markdown file using the following shortcode: **{{&lt; youtube &quot;6BsxSJJb6-g&quot; &gt;}}**

{{< youtube "6BsxSJJb6-g" >}}

## Resolution

To use the thumbnail with the highest available resolution, use the second
positional parameter and set the value to `"max"`.

{{< youtube "6BsxSJJb6-g" "max" >}}
