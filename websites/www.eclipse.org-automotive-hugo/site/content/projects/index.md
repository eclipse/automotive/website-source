---
date: 2021-04-30
title: "Projects"
weight: 5
hide_sidebar: "false"
---

<section id="block-project-blocks-related-projects" class="block block-project-blocks block-region-sidebar-second block-solstice-collapse solstice-block solstice-block-default solstice-block-white-bg block-related-projects solstice-block-white-bg clearfix">

<h2 class="block-title">Related Projects</h2>

  <div class="block-content">
    <h3 class="h5">Project Hierarchy:</h3>
    <ul class="related-projects-block-content-hierarchy-list">
      <li class="ellipsis hierarchy-1"><a href="https://projects.eclipse.org/projects/automotive" class="node-reference-hierarchy-0">Eclipse Automotive</a></li>
      <ul class="related-projects-block-content-hierarchy-list">
        <li class="ellipsis hierarchy-2"><a href="https://projects.eclipse.org/projects/automotive.app4mc" class="node-reference-hierarchy-1">Eclipse APP4MC</a></li>
        <li class="ellipsis hierarchy-2"><a href="https://projects.eclipse.org/projects/automotive.adore" class="node-reference-hierarchy-1">Eclipse Automated Driving Open Research (ADORe)</a></li>
        <li class="ellipsis hierarchy-2"><a href="https://projects.eclipse.org/projects/automotive.cloe" class="node-reference-hierarchy-1">Eclipse Cloe</a></li>
        <li class="ellipsis hierarchy-2"><a href="https://projects.eclipse.org/projects/automotive.kuksa" class="node-reference-hierarchy-1">Eclipse Kuksa</a></li>
        <li class="ellipsis hierarchy-2"><a href="https://projects.eclipse.org/projects/automotive.mdmbl" class="node-reference-hierarchy-1">Eclipse MDM|BL</a></li>
        <li class="ellipsis hierarchy-2"><a href="https://projects.eclipse.org/projects/automotive.mosaic" class="node-reference-hierarchy-1">Eclipse MOSAIC</a></li>
        <li class="ellipsis hierarchy-2"><a href="https://projects.eclipse.org/projects/automotive.openmcx" class="node-reference-hierarchy-1">Eclipse OpenMCx</a></li>
        <li class="ellipsis hierarchy-2"><a href="https://projects.eclipse.org/projects/automotive.simopenpass" class="node-reference-hierarchy-1">Eclipse sim@openPASS™</a></li>
        <li class="ellipsis hierarchy-2"><a href="https://projects.eclipse.org/projects/automotive.sphinx" class="node-reference-hierarchy-1">Eclipse Sphinx™</a></li>
        <li class="ellipsis hierarchy-2"><a href="https://projects.eclipse.org/projects/automotive.sumo" class="node-reference-hierarchy-1">Eclipse SUMO™</a></li>
        <li class="ellipsis hierarchy-2"><a href="https://projects.eclipse.org/projects/automotive.tractusx" class="node-reference-hierarchy-1">Eclipse Tractus-X</a></li>
      </ul>
    </ul>
  </div>
</section> <!-- /.block -->
