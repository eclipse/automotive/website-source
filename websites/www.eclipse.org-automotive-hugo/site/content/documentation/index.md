---
title: "Documentation"
date: 2021-05-11
hide_sidebar: "false"
---


Sign up on the [Automotive mailing list](https://accounts.eclipse.org/mailing-list/automotive-pmc) for the latest news.


### Links

* [Top-Level Project Charter](https://projects.eclipse.org/projects/automotive/charter)

* [Wiki](https://wiki.eclipse.org/Automotive)
