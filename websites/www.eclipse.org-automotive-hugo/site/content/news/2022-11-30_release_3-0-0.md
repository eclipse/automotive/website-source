---
date: 2022-11-30
title: "APP4MC - Release 3.0.0 published"
categories: ["news"]
slug: "2022-11-30-release-3-3-0"
---

We released a new version of APP4MC with new features and improvements.

<!--more-->

__Extract of APP4MC 3.0.0 Release notes__


Model handling

	Model migration support (2.2.0 -> 3.0.0)


Model

    Removed deprecated model elements (ModeCondition* and ModeSwitch*)
    Simplified data type definitions (inline type definition is no longer supported)
    New lock type "Mutex"
    New lock attribute "ownership"


Product

	
    Extended model documentation and validation of semaphores
    Updated visualization framework:
    * New utility classes for SVG (PlantUML, GraphViz)
    * New persisted visualization parameters (per view)
    * New visualization: EObject references
    * Updated visualizations: Hardware, Runnable dependencies
    Several bug fixes


Recommended Java runtime is **Java 11**.  
Minimum is Java 8 (with limitations if JavaFX is not included).

__Developer info__

* Amalthea components (model, validation, migration) are available at **[Maven Central](https://search.maven.org/search?q=app4mc)**

__Further details__

* See slides [New & Noteworthy](https://archive.eclipse.org/app4mc/documents/news/APP4MC_3.0.0_New_and_noteworthy.pdf)
* Visit the [APP4MC download page](https://projects.eclipse.org/projects/automotive.app4mc/downloads)
